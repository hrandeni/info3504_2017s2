/*
 * dnatype.h
 *
 *  Created on: 6 Sep.,2017
 *      Author: Harshana Randeni
 *
 *  Modified: 13 Sep.,2017
 *  
 *
 *  Used isax.h as inspiration
  */

#ifndef __DNATYPE_H__
#define __DNATYPE_H__

#define ALPHABETSIZE 5
#define GAPCOST 8
/*
 *
 */
typedef struct DNATYPE {
    // TODO for students
    // Define a DNATYPE that holds the alphabet {A,G,C,T, N}
    bool tempValue; // this is temporary holder value, you can remove it as it has no purpose
} DNATYPE;

void parse_dna_sequence_string(char*, DNATYPE*);
char* dna_sequence_to_string(DNATYPE*);
Datum similarity_score_sw(FunctionCallInfo);
Datum dnatype_eq(FunctionCallInfo);

#endif /* __DNATYPE_H__ */