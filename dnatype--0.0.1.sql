-- created by Harshana Randeni 2017-09-06 for University of Sydney INFO3504 assignment
-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION dnatype" to load this file. \quit

CREATE FUNCTION dnatype_in(cstring)
RETURNS dnatype
AS '$libdir/dnatype'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION dnatype_out(dnatype)
RETURNS cstring
AS '$libdir/dnatype'
LANGUAGE C IMMUTABLE STRICT;

CREATE TYPE dnatype (
  INPUT          = dnatype_in,
  OUTPUT         = dnatype_out  -- students may add to the type definition if they wish
);

CREATE FUNCTION similarity_score_sw(dnatype) 
RETURNS int4
AS '$libdir/dnatype'
LANGUAGE C IMMUTABLE STRICT;

CREATE FUNCTION dnatype_eq(dnatype, dnatype)
RETURNS boolean 
AS '$libdir/dnatype'
LANGUAGE C IMMUTABLE STRICT;

CREATE OPERATOR = (
  LEFTARG = dnatype,
  RIGHTARG = dnatype,
  PROCEDURE = dnatype_eq,
  COMMUTATOR = '=',
  NEGATOR = '<>',
  RESTRICT = eqsel,
  JOIN = eqjoinsel,
  HASHES, MERGES
);