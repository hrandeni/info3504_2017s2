# created by Harshana Randeni 2017-09-06 for University of Sydney INFO3504 assignment
EXTENSION = dnatype        # the extensions name
DATA = dnatype--0.0.1.sql  # script files to install
REGRESS = dnatype_test     # our test script file (without extension)
MODULES = dnatype          # our c module file to build

# postgres build stuff
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
