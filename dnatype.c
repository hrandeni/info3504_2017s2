/*
 * dnatype.c
 * 
 * created by Harshana Randeni 2017-09-06 for University of Sydney INFO3504 assignment
 * Modified 2017-09-13 by Harshana Randeni 
 * - added PG_FUNCTION_INFO_V1() for dnatype_eq and similarity_score_sw to allow the code to compile
 * - changed PG_RETURN_INT4 to PG_RETURN_INT32 to allow code to compile
 * 
 * used dnatype.c and isax.c as inspiration
*/

#include "postgres.h"
#include "fmgr.h"
#include "utils/builtins.h"
#include "dnatype.h"

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(dnatype_in);

Datum
dnatype_in(PG_FUNCTION_ARGS)
{
    DNATYPE *result;
    char *str = PG_GETARG_CSTRING(0);
    parse_dna_sequence_string(str, result);
    PG_RETURN_POINTER(result);
}

PG_FUNCTION_INFO_V1(dnatype_out);
Datum
dnatype_out(PG_FUNCTION_ARGS)
{
    DNATYPE *our_dnatype = (DNATYPE *) PG_GETARG_POINTER(0);
    char    *result;

    result = dna_sequence_to_string(our_dnatype);

    PG_RETURN_CSTRING(result);
}

void parse_dna_sequence_string(char* sequencestring, DNATYPE* result ) {
    // TODO Students - parse the DNA sequence string and populate the DNATYPE result
    // remember to allocate sufficient space for your DNATYPE
}

char* dna_sequence_to_string(DNATYPE* our_dnatype) {
    //TODO students - parse the DNATYPE back into a string representation
}

/*
 * Extra functions
 */

 /*
  * A check to see if a DNATYPE is the same as another DNATYPE
  */
PG_FUNCTION_INFO_V1(dnatype_eq);

Datum
dnatype_eq(PG_FUNCTION_ARGS)
{
    DNATYPE *leftarg_dnatype = (DNATYPE *) PG_GETARG_POINTER(0);
    DNATYPE *rightarg_dnatype = (DNATYPE *) PG_GETARG_POINTER(1);
    bool result = false;

    // TODO Students - check to see if the the types are equal

    PG_RETURN_BOOL(result);
}

 /*
  * Calculate the maximum similarity score between two dnatypes based on the Smith Waterman algorithm.
  */
PG_FUNCTION_INFO_V1(similarity_score_sw);

Datum
similarity_score_sw(PG_FUNCTION_ARGS)
{
    int result = 0;

    DNATYPE *leftarg_dnatype = (DNATYPE *) PG_GETARG_POINTER(0);
    DNATYPE *rightarg_dnatype = (DNATYPE *) PG_GETARG_POINTER(1);

    // TODO Students - give the maximum similarity score between two dnatypes based on the Smith Waterman algorithm

    PG_RETURN_INT32(result);
}

 /*
  * Give a complementary sequence to the current DNA sequence
  */
  // Extra task - need to define what a complementary sequence is
// Datum
// complement(PG_FUNCTION_ARGS)
// {
//     DNATYPE *result;
//     DNATYPE *our_dnatype = (DNATYPE *) PG_GETARG_POINTER(0);

//     //TODO Students - Generate a complementary DNA sequence to the supplied one

//     PG_RETURN_POINTER(result);
// }

