# README #

INFO3504 Assignment 2017 S2

### Coding Tasks ###

* Develop a datatype for storing DNA sequence data
* Complete the '=' Operator
* Complete the similarity\_score\_sw function

### Testing Tasks ###

* Think about what tests you need to test your type, operator and function

### Report ###
* Complete your report
